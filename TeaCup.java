package cs102;

public class TeaCup {
    public double Vol;
    public int Temp;
    public boolean Lemon;

    public TeaCup(double vol,int temp,boolean lem){
        Vol = vol;
        Temp = temp;
        Lemon = lem;
    }
    public void incTemp(int i){
        this.Temp += i;
    }
    public void decTemp(int i){
        this.Temp -= i;
    }
    public void incVol(double vol){
        this.Vol +=vol;
    }
    public void existLem(){
        this.Lemon=true;
    }
    public void notExistLem(){
        this.Lemon=false;
    }
    public void report(int a){
        System.out.println("State " + a);
        String lem;
        System.out.println("Volume is " + this.Vol + " liters");
        System.out.println("Temperature is " + this.Temp + " degree");

        if(this.Lemon == true){
            lem = "exist";
        }else{
            lem = "does not exist";
        }
        System.out.println("Lemon is " + lem);


    }

}
