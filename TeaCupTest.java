package cs102;

public class TeaCupTest {
    public static void main(String[] args){
        TeaCup t1 = new TeaCup(0.1,30,false);
        t1.report(1);
        t1.incTemp(20);
        t1.report(2);
        t1.decTemp(20);
        t1.report(3);
        t1.incVol(0.1);
        t1.report(4);
        t1.existLem();
        t1.report(5);
        t1.notExistLem();
        t1.report(6);
    }

}
